package com.classowl.app.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;

import com.classowl.app.ClassOwlApplication;
import com.classowl.app.activities.RegisterActivity;
import com.classowl.app.model.School;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;

/**
 * Loads asynchronously school data.
 */
public class LoadSchoolsTask extends AsyncTask<Void, Void, Void> {

    private final static String URL_SCHOOLS = "https://api.classowl.com/v1/schools?format=json";
    private Activity context;

    public LoadSchoolsTask(Activity context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(URL_SCHOOLS);

        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                String json = EntityUtils.toString(response.getEntity());
                loadSchoolsIntoApp(json);
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void loadSchoolsIntoApp(String json) {
        List<School> schoolList = Parser.convertJsonToListOfSchool(json);
        ClassOwlApplication app = (ClassOwlApplication) context.getApplication();
        app.setSchools(schoolList);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        goToRegisterScreen();
        super.onPostExecute(aVoid);
    }

    private void goToRegisterScreen() {
        Intent registerIntent = new Intent(context, RegisterActivity.class);
        context.startActivity(registerIntent);
        context.finish();
    }
}
