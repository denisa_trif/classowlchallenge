package com.classowl.app.utils;

import com.classowl.app.model.JsonClassOwlSchoolsV1;
import com.classowl.app.model.School;
import com.classowl.app.model.User;
import com.google.gson.Gson;

import java.util.List;

public class Parser {

    public static List<School> convertJsonToListOfSchool(String json) {
        Gson gson = new Gson();
        JsonClassOwlSchoolsV1 parsedJson = gson.fromJson(json, JsonClassOwlSchoolsV1.class);
        List<School> schools = parsedJson.getObjects();
        return schools;
    }

    public static String convertUserToJson(User user) {
        Gson gson = new Gson();
        return gson.toJson(user);
    }
}
