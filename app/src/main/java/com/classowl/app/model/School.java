package com.classowl.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class School {

    @SerializedName("auth_url")
    private String authUrl;

    @SerializedName("current_term")
    private String currentTerm;

    @SerializedName("description")
    private String description;

    @SerializedName("email")
    private String email;

    @SerializedName("has_departments")
    private Boolean hasDepartments;

    @SerializedName("has_syllabi")
    private Boolean hasSyllabi;

    @SerializedName("honor_code")
    private String honorCode;

    @SerializedName("id")
    private Integer id;

    @SerializedName("identifier")
    private String identifier;

    @SerializedName("is_active")
    private Boolean isActive;

    @SerializedName("is_scraped")
    private Boolean isScraped;

    @SerializedName("location")
    private String location;

    @SerializedName("name")
    private String name;

    @SerializedName("resource_url")
    private String resourceUrl;

    @SerializedName("terms")
    private List<String> terms;

    @SerializedName("type")
    private String type;

    public School() {
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }

    public String getCurrentTerm() {
        return currentTerm;
    }

    public void setCurrentTerm(String currentTerm) {
        this.currentTerm = currentTerm;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getHasDepartments() {
        return hasDepartments;
    }

    public void setHasDepartments(Boolean hasDepartments) {
        this.hasDepartments = hasDepartments;
    }

    public Boolean getHasSyllabi() {
        return hasSyllabi;
    }

    public void setHasSyllabi(Boolean hasSyllabi) {
        this.hasSyllabi = hasSyllabi;
    }

    public String getHonorCode() {
        return honorCode;
    }

    public void setHonorCode(String honorCode) {
        this.honorCode = honorCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsScraped() {
        return isScraped;
    }

    public void setIsScraped(Boolean isScraped) {
        this.isScraped = isScraped;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public List<String> getTerms() {
        return terms;
    }

    public void setTerms(List<String> terms) {
        this.terms = terms;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
