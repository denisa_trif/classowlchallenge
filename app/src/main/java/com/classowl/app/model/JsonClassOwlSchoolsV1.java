package com.classowl.app.model;

import java.util.List;

public class JsonClassOwlSchoolsV1 {
    private List<School> objects;
    private Metadata meta;

    public Metadata getMeta() {
        return meta;
    }

    public void setMeta(Metadata meta) {
        this.meta = meta;
    }

    public List<School> getObjects() {
        return objects;
    }

    public void setObjects(List<School> o) {
        objects = o;
    }
}
