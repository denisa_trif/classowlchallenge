package com.classowl.app.activities;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.classowl.app.ClassOwlApplication;
import com.classowl.app.R;
import com.classowl.app.model.School;
import com.classowl.app.model.User;
import com.classowl.app.utils.Parser;

import java.util.ArrayList;
import java.util.List;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;

/**
 * Inherites RoboActivity to use the dependency injection - library downloaded via gradle.
 */
public class RegisterActivity extends RoboActivity {

    @InjectView(R.id.first_name_text)
    EditText firstNameEdittext;

    @InjectView(R.id.second_name_text)
    EditText lastNameEdittext;

    @InjectView(R.id.first_spinner)
    Spinner schoolSpinner;

    @InjectView(R.id.second_spinner)
    Spinner iAmSpinner;

    @InjectView(R.id.email_text)
    EditText emailEdittext;

    @InjectView(R.id.password_text)
    EditText passwordEdittext;

    @InjectView(R.id.sign_up_button)
    Button signUpButton;

    @InjectResource(R.array.i_am_values)
    String[] positionValues;

    List<School> schools;

    ClassOwlApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_screen);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        app = (ClassOwlApplication) this.getApplication();
        schools = app.getSchools();
        setupSpinnersData();
    }

    /**
     * Dynamically setting of the spinners' entries and layouts.
     */
    private void setupSpinnersData() {
        List<String> schoolNames = new ArrayList<String>();
        if (schools != null) {
            for (School s : schools) {
                schoolNames.add(s.getName());
            }
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, schoolNames);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        schoolSpinner.setAdapter(dataAdapter);

        ArrayAdapter<String> positionSpinnerAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, positionValues);
        positionSpinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        iAmSpinner.setAdapter(positionSpinnerAdapter);
    }

    /**
     * Sign up button handler, as specified in the onClick attribute in xml.
     * @param v the clicked view
     */
    public void signUp(View v) {
        if (v.getId() == R.id.sign_up_button) {
            if (isFilled()) {
                registerUser();
            } else {
                Toast.makeText(this, getString(R.string.please_fill_toast), Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Adds an user to the list held by the application's state and prints the user.
     * In the future, it should be stored in a DB or handled through an API.
     */
    private void registerUser() {
        User newUser = new User();
        newUser.setSchoolId(schools.get(schoolSpinner.getSelectedItemPosition()).getId());
        newUser.setPosition(iAmSpinner.getSelectedItem().toString());
        newUser.setEmail(emailEdittext.getText().toString());
        newUser.setFirstName(firstNameEdittext.getText().toString());
        newUser.setLastName(lastNameEdittext.getText().toString());
        newUser.setPassword(passwordEdittext.getText().toString());
        app.addToUsers(newUser);
        printUserJsonFormat(newUser);
    }

    private void printUserJsonFormat(User u) {
        System.out.println(Parser.convertUserToJson(u));
    }

    /**
     * Checks if all the fields are filled.
     * @return true if all the fields are filled, false otherwise.
     */
    private boolean isFilled() {
        String schoolSelected = schoolSpinner.getSelectedItem().toString();
        String positionSelected = iAmSpinner.getSelectedItem().toString();
        String firsName = firstNameEdittext.getText().toString();
        String lastName = lastNameEdittext.getText().toString();
        String email = emailEdittext.getText().toString();
        String password = passwordEdittext.getText().toString();
        if (schoolSelected == null || positionSelected == null || firsName == null ||
                lastName == null || email == null || password == null) {
            return false;
        }
        if (firsName.equals("") || lastName.equals("") || email.equals("") || password.equals("")) {
            return false;
        }
        return true;
    }
}