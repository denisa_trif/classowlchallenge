package com.classowl.app.activities;

import android.app.Activity;
import android.os.Bundle;

import com.classowl.app.ClassOwlApplication;
import com.classowl.app.R;
import com.classowl.app.model.School;
import com.classowl.app.model.User;
import com.classowl.app.utils.LoadSchoolsTask;

import java.util.ArrayList;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        initData();
        new LoadSchoolsTask(this).execute();
    }

    /**
     * Read users and other data from a server/db etc.
     */
    private void initData() {
        ClassOwlApplication app = (ClassOwlApplication) getApplication();
        app.setUsers(new ArrayList<User>());
        app.setSchools(new ArrayList<School>());
    }
}
