package com.classowl.app;

import android.app.Application;

import com.classowl.app.model.School;
import com.classowl.app.model.User;

import java.util.List;

/**
 * Stores the application state.
 */
public class ClassOwlApplication extends Application {

    private List<School> schools;
    private List<User> users;

    public void setSchools(List<School> l) {
        schools = l;
    }

    public List<School> getSchools() {
        return schools;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void addToUsers(User u) {
        users.add(u);
    }
}
